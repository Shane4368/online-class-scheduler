# Online Class Scheduler

A WinUI 3 Desktop app to show toast notifications for your online classes.

![](Images/app-screenshot.png)
![](Images/app-toast-notification-screenshot.png)

## Exporting

Your entries can be exported to an iCalendar file (.ics).
If you decide to import this file into Google Calendar, event notifications are ignored by default.
To allow event notifications on your imported calendar, you need to follow these steps
in exact order:

1. Create new calendar
2. Enable event notifications
3. Import the .ics file