﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace OnlineClassScheduler.BackgroundService;

internal static class Program
{
	private static SqliteConnection _connection = null!;

	private static async Task Main(string[] args)
	{
		_connection = CreateConnection(args[1]);

		while (true)
		{
			await foreach (var onlineClass in GetOnlineClasses())
			{
				var dateTimeNow = DateTime.Now;
				var currentDay = dateTimeNow.DayOfWeek;
				var triggerDay = Enum.Parse<DayOfWeek>(onlineClass.Day);

				var triggerDateTime = dateTimeNow.Date
					.AddDays(triggerDay - currentDay)
					.Add(TimeSpan.Parse(onlineClass.Time))
					.AddMinutes(-onlineClass.MinutesAhead);

				if (triggerDateTime.DayOfWeek == currentDay &&
					triggerDateTime.Hour == dateTimeNow.Hour &&
					triggerDateTime.Minute == dateTimeNow.Minute)
					SendNotification(args[0], onlineClass.Id);
			}

			await Task.Delay(60 * 1000);
		}
	}



	private static SqliteConnection CreateConnection(string dataSource)
	{
		var connectionStringBuilder = new SqliteConnectionStringBuilder
		{
			DataSource = dataSource,
			Mode = SqliteOpenMode.ReadOnly
		};

		return new(connectionStringBuilder.ToString());
	}



	private static async IAsyncEnumerable<OnlineClass> GetOnlineClasses()
	{
		_connection.Open();

		int appMinutesAhead = 0;
		using var command = _connection.CreateCommand();

		command.CommandText = "SELECT minutes_ahead FROM app";

		var reader = command.ExecuteReader();

		if (reader.Read())
			appMinutesAhead = reader.GetInt32(0);

		await reader.DisposeAsync();

		command.CommandText = "SELECT id, [day], [time], minutes_ahead FROM online_class";

		reader = command.ExecuteReader();

		while (reader.Read())
		{
			var minutesAhead = reader.GetInt32(3);

			yield return new OnlineClass
			{
				Id = reader.GetString(0),
				Day = reader.GetString(1),
				Time = reader.GetString(2),
				MinutesAhead = minutesAhead == 0 ? appMinutesAhead : minutesAhead
			};
		}

		await reader.DisposeAsync();

		_connection.Close();
	}



	private static void SendNotification(string packageFamilyName, string onlineClassId)
	{
		Process.Start(new ProcessStartInfo
		{
			FileName = "powershell.exe",
			Arguments = @$"Start-Process shell:AppsFolder\{packageFamilyName}!App -ArgumentList {onlineClassId}",
			CreateNoWindow = true
		})!.Dispose();
	}
}
