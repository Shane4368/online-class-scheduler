﻿namespace OnlineClassScheduler.BackgroundService;

internal sealed class OnlineClass
{
	public string Id { get; init; } = null!;
	public string Day { get; init; } = null!;
	public string Time { get; init; } = null!;
	public int MinutesAhead { get; init; }
}
