﻿using System;
using System.IO;
using Microsoft.Data.Sqlite;

namespace OnlineClassScheduler.Services;

public static class DatabaseFactory
{
	private static SqliteConnection? _connection;

	public static SqliteConnection getConnection()
	{
		if (_connection == null)
		{
			var saveLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
				@$"\{nameof(OnlineClassScheduler)}.db";

			_connection = new SqliteConnection("Data Source=" + saveLocation);
			_connection.Open();

			using var command = _connection.CreateCommand();

			command.CommandText = File.ReadAllText(
				AppDomain.CurrentDomain.BaseDirectory + @"\create-tables.sql");

			_ = command.ExecuteNonQuery();

			_connection.Close();
		}

		return _connection;
	}
}
