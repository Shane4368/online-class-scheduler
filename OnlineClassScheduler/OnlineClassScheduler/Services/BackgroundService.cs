﻿using System.Diagnostics;
using IWshRuntimeLibrary;
using OnlineClassScheduler.Configuration;

namespace OnlineClassScheduler.Services;

public static class BackgroundService
{
	private const string FolderName = "OnlineClassScheduler (Background)";

	public static string GetExecutablePath()
	{
		return $"{AppConfiguration.PackageInstalledLocation.Path}/{FolderName}/{FolderName}.exe";
	}

	public static void EnsureCreatedTask()
	{
		// https://stackoverflow.com/a/4909475
		var shell = new WshShell();
		var shortcutPathLink = shell.SpecialFolders.Item("Startup") + @$"\{FolderName}.lnk";

		if (System.IO.File.Exists(shortcutPathLink)) return;

		var shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPathLink);
		var arguments = @$"Start-Process shell:AppsFolder\{AppConfiguration.PackageFamilyName}!App -ArgumentList --start-background-service";

		shortcut.Description = "Launches background process for Online Class Scheduler.";
		shortcut.TargetPath = "powershell.exe";
		shortcut.Arguments = arguments;
		shortcut.WindowStyle = 7; // minimized
		shortcut.Save();

		Process.Start(new ProcessStartInfo
		{
			FileName = shortcut.TargetPath,
			Arguments = arguments,
			CreateNoWindow = true
		})!.Dispose();
	}
}
