﻿using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using OnlineClassScheduler.Extensions;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Services;

public sealed class DataGridColumnSettingsQuery
{
	private static readonly SqliteConnection _connection = DatabaseFactory.getConnection();

	public void Save(IEnumerable<DataGridColumnSettings> dataGridColumnSettingsList, bool createNewRecords)
	{
		_connection.Open();

		if (createNewRecords)
			foreach (var settings in dataGridColumnSettingsList)
				this.UnsafeInsert(settings);
		else
			foreach (var settings in dataGridColumnSettingsList)
				this.UnsafeUpdate(settings);

		_connection.Close();
	}

	public IReadOnlyCollection<DataGridColumnSettings> SelectAll()
	{
		var dataGridColumnSettingsList = new List<DataGridColumnSettings>();

		_connection.Open();

		using var command = _connection.CreateCommand();
		command.CommandText = "SELECT * FROM datagrid_column";

		using var reader = command.ExecuteReader();

		while (reader.Read())
		{
			dataGridColumnSettingsList.Add(reader.GetDataGridColumnSettings());
		}

		_connection.Close();
		return dataGridColumnSettingsList.AsReadOnly();
	}

	private void UnsafeInsert(DataGridColumnSettings settings)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "INSERT INTO datagrid_column VALUES (@name, @visibility, @index)";

		this.AddCommandParametersWithValue(command.Parameters, settings);
		_ = command.ExecuteNonQuery();
	}

	private void UnsafeUpdate(DataGridColumnSettings settings)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "UPDATE datagrid_column SET visibility=@visibility, display_index=@index WHERE [name]=@name";

		this.AddCommandParametersWithValue(command.Parameters, settings);
		_ = command.ExecuteNonQuery();
	}

	private void AddCommandParametersWithValue(SqliteParameterCollection collection, DataGridColumnSettings settings)
	{
		_ = collection.AddWithValue("name", settings.ColumnName);
		_ = collection.AddWithValue("visibility", (int)settings.Visibility);
		_ = collection.AddWithValue("index", settings.DisplayIndex);
	}
}
