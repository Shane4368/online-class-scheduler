﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.Sqlite;
using OnlineClassScheduler.Extensions;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Services;

public sealed class OnlineClassQuery
{
	private static readonly SqliteConnection _connection = DatabaseFactory.getConnection();

	public static OnlineClass SelectFromDatabase(string id)
	{
		_connection.Open();

		using var command = _connection.CreateCommand();
		command.CommandText = "SELECT * FROM online_class WHERE id=@id";

		_ = command.Parameters.AddWithValue("id", id);

		using var reader = command.ExecuteReader();
		_ = reader.Read();

		var onlineClass = reader.GetOnlineClass();

		_connection.Dispose();
		return onlineClass;
	}



	private List<OnlineClass>? _addedOnlineClasses;
	private List<OnlineClass>? _removedOnlineClasses;

	public void Add(OnlineClass onlineClass)
	{
		this._addedOnlineClasses ??= new List<OnlineClass>();
		this._addedOnlineClasses.Add(onlineClass);
	}

	public void Remove(IEnumerable<OnlineClass> onlineClasses)
	{
		this._removedOnlineClasses ??= new List<OnlineClass>();
		this._removedOnlineClasses.AddRange(onlineClasses);
	}

	public void Save(IEnumerable<OnlineClass> onlineClasses)
	{
		if (!onlineClasses.Any())
		{
			this.DeleteAll();
			this._addedOnlineClasses?.Clear();
			this._removedOnlineClasses?.Clear();
			return;
		}

		_connection.Open();

		this._addedOnlineClasses?.ForEach(x =>
		{
			// left-hand side is bool? which can be null
			if (this._removedOnlineClasses?.Contains(x) != true)
				this.UnsafeInsert(x);
		});

		this._removedOnlineClasses?.ForEach(x => this.UnsafeDelete(x.Id));

		foreach (var onlineClass in onlineClasses)
			if (onlineClass.IsModified)
				this.UnsafeUpdate(onlineClass);

		this._addedOnlineClasses?.Clear();
		this._removedOnlineClasses?.Clear();

		_connection.Close();
	}



	public IReadOnlyCollection<OnlineClass> SelectAll()
	{
		var onlineClasses = new List<OnlineClass>();

		_connection.Open();

		using var command = _connection.CreateCommand();
		command.CommandText = "SELECT * FROM online_class";

		using var reader = command.ExecuteReader();

		while (reader.Read())
			onlineClasses.Add(reader.GetOnlineClass());

		_connection.Close();
		return onlineClasses.AsReadOnly();
	}

	private void UnsafeInsert(OnlineClass onlineClass)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "INSERT INTO online_class VALUES (@id, @occ, @mcode, @mname, @lecturer, @type, @day, @time, @uri, @minutes_ahead)";

		this.AddCommandParametersWithValue(command.Parameters, onlineClass);
		_ = command.ExecuteNonQuery();

		onlineClass.IsModified = false;
	}

	private void UnsafeUpdate(OnlineClass onlineClass)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "UPDATE online_class SET occurrence=@occ, module_code=@mcode, module_name=@mname, lecturer=@lecturer, [type]=@type, [day]=@day, [time]=@time, uri=@uri, minutes_ahead=@minutes_ahead WHERE id=@id";

		this.AddCommandParametersWithValue(command.Parameters, onlineClass);
		_ = command.ExecuteNonQuery();

		onlineClass.IsModified = false;
	}

	private void UnsafeDelete(string id)
	{
		using var command = _connection.CreateCommand();

		command.CommandText = "DELETE FROM online_class WHERE id=@id";

		_ = command.Parameters.AddWithValue("id", id);
		_ = command.ExecuteNonQuery();
	}

	private void DeleteAll()
	{
		_connection.Open();

		using var command = _connection.CreateCommand();
		command.CommandText = "DELETE FROM online_class";
		_ = command.ExecuteNonQuery();

		_connection.Close();
	}

	private void AddCommandParametersWithValue(SqliteParameterCollection collection, OnlineClass onlineClass)
	{
		_ = collection.AddWithValue("id", onlineClass.Id);
		_ = collection.AddWithSafeValue("occ", onlineClass.Occurrence);
		_ = collection.AddWithSafeValue("mcode", onlineClass.ModuleCode);
		_ = collection.AddWithValue("mname", onlineClass.ModuleName);
		_ = collection.AddWithSafeValue("lecturer", onlineClass.LecturerName);
		_ = collection.AddWithSafeValue("type", onlineClass.Type);
		_ = collection.AddWithValue("day", onlineClass.Day);
		_ = collection.AddWithValue("time", onlineClass.Time);
		_ = collection.AddWithValue("uri", onlineClass.Uri);
		_ = collection.AddWithValue("minutes_ahead", onlineClass.MinutesAhead);
	}
}
