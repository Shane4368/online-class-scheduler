﻿using Microsoft.Data.Sqlite;
using OnlineClassScheduler.Configuration;
using OnlineClassScheduler.Extensions;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Services;

public sealed class AppSettingsQuery
{
	private static readonly SqliteConnection _connection = DatabaseFactory.getConnection();

	public void Save(AppSettings appSettings, bool createNewRecord)
	{
		_connection.Open();

		if (createNewRecord)
			this.UnsafeInsert(appSettings);
		else
			this.UnsafeUpdate(appSettings);

		_connection.Close();
	}

	public AppSettings? Select()
	{
		_connection.Open();

		using var command = _connection.CreateCommand();
		command.CommandText = "SELECT * FROM app WHERE id=@id";

		_ = command.Parameters.AddWithValue("id", AppConfiguration.PackageName);

		using var reader = command.ExecuteReader();

		if (reader.Read())
		{
			var appSettings = reader.GetAppSettings();

			_connection.Close();
			return appSettings;
		}

		_connection.Close();
		return null;
	}

	private void UnsafeInsert(AppSettings appSettings)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "INSERT INTO app VALUES (@id, @use_military_time, @theme, @minutes_ahead)";

		this.AddCommandParametersWithValue(command.Parameters, appSettings);
		_ = command.ExecuteNonQuery();
	}

	private void UnsafeUpdate(AppSettings appSettings)
	{
		using var command = _connection.CreateCommand();
		command.CommandText = "UPDATE app SET use_military_time=@use_military_time, theme=@theme, minutes_ahead=@minutes_ahead WHERE id=@id";

		this.AddCommandParametersWithValue(command.Parameters, appSettings);
		_ = command.ExecuteNonQuery();
	}

	private void AddCommandParametersWithValue(SqliteParameterCollection collection, AppSettings appSettings)
	{
		_ = collection.AddWithValue("id", AppConfiguration.PackageName);
		_ = collection.AddWithValue("use_military_time", appSettings.UseMilitaryTime);
		_ = collection.AddWithValue("theme", appSettings.ColorTheme);
		_ = collection.AddWithValue("minutes_ahead", appSettings.MinutesAhead);
	}
}
