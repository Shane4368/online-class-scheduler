﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Services;

public sealed class QueryCollection
{
	private readonly AppSettingsQuery _appSettingsQuery = new();
	private readonly DataGridColumnSettingsQuery _dataGridColumnSettingsQuery = new();
	private readonly OnlineClassQuery _onlineClassQuery = new();

	private AppSettings? _appSettings;
	private ObservableCollection<OnlineClass>? _onlineClasses;
	private IReadOnlyCollection<DataGridColumnSettings>? _dataGridColumnSettingsList;



	/**********************************************************************
	 * APPSETTINGS
	 */
	public AppSettings GetAppSettings()
	{
		if (this._appSettings == null)
		{
			var appSettings = this._appSettingsQuery.Select();

			if (appSettings == null)
			{
				appSettings = new AppSettings();
				this._appSettingsQuery.Save(appSettings, true);
			}

			appSettings.PropertyChanged += (_, _) => this.SaveAppSettings();
			this._appSettings = appSettings;
		}

		return this._appSettings;
	}

	public void SaveAppSettings()
	{
		this._appSettingsQuery.Save(this._appSettings!, false);
	}



	/**********************************************************************
	 * DATAGRIDCOLUMNSETTINGS
	 */
	public IReadOnlyCollection<DataGridColumnSettings> GetDataGridColumnSettingsList()
	{
		if (this._dataGridColumnSettingsList == null)
		{
			var results = this._dataGridColumnSettingsQuery.SelectAll();
			this._dataGridColumnSettingsList = results;
		}

		return this._dataGridColumnSettingsList;
	}

	public void SetDataGridColumnSettingsList(IReadOnlyCollection<DataGridColumnSettings> collection)
	{
		this._dataGridColumnSettingsList = collection;
	}

	public void SaveDataGridColumnSettingsList(bool createNewRecords)
	{
		this._dataGridColumnSettingsQuery.Save(this._dataGridColumnSettingsList!, createNewRecords);
	}



	/**********************************************************************
	 * ONLINECLASS
	 */
	public ObservableCollection<OnlineClass> GetOnlineClasses()
	{
		if (this._onlineClasses == null)
		{
			var results = this._onlineClassQuery.SelectAll();
			this._onlineClasses = new ObservableCollection<OnlineClass>(results);
		}

		return this._onlineClasses;
	}

	public void SaveOnlineClasses()
	{
		this._onlineClassQuery.Save(this._onlineClasses!);
	}

	public void AddOnlineClass(OnlineClass onlineClass)
	{
		this._onlineClasses!.Add(onlineClass);
		this._onlineClassQuery.Add(onlineClass);
	}

	public int RemoveOnlineClasses(IEnumerable<OnlineClass> onlineClasses)
	{
		this._onlineClassQuery.Remove(onlineClasses);

		var count = onlineClasses.Count();

		for (int i = 0; i < count; i++)
			this._onlineClasses!.Remove(onlineClasses.First());

		return count;
	}

	public int RemoveOnlineClasses()
	{
		var count = this._onlineClasses!.Count;
		this._onlineClassQuery.Remove(this._onlineClasses!);
		this._onlineClasses!.Clear();
		return count;
	}
}
