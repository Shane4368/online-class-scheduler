﻿using System;
using System.Globalization;
using Microsoft.UI.Xaml.Data;

namespace OnlineClassScheduler.Converters;

public sealed class StringToTimeSpanConverter : IValueConverter
{
	public object? Convert(object value, Type targetType, object parameter, string language)
	{
		return value != null
			? TimeSpan.Parse(value.ToString()!)
			: null;
	}

	public object? ConvertBack(object value, Type targetType, object parameter, string language)
	{
		return value != null
			? ((TimeSpan)value).ToString("h\\:mm", new CultureInfo("en-US"))
			: null;
	}
}
