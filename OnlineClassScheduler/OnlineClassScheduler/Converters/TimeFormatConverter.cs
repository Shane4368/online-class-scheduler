﻿using System;
using Microsoft.UI.Xaml.Data;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Converters;

public sealed class TimeFormatConverter : IValueConverter
{
	public object? Convert(object value, Type targetType, object parameter, string language)
	{
		if (value == null) return null;

		var appSettings = (AppSettings)parameter;

		if (!appSettings.UseMilitaryTime)
		{
			// use 12 hour format
			return TimeOnly.Parse(value.ToString()!).ToString("h:mm tt");
		}

		return value;
	}

	public object? ConvertBack(object value, Type targetType, object parameter, string language)
	{
		if (!TimeOnly.TryParse(value.ToString(), out var timeOnly)) return null;

		var appSettings = (AppSettings)parameter;

		if (!appSettings.UseMilitaryTime)
		{
			// revert to 24 hour format
			return timeOnly.ToString("H:mm");
		}

		return value;
	}
}
