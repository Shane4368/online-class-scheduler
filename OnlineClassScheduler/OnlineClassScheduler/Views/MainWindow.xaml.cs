﻿using System;
using System.Collections.Generic;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using OnlineClassScheduler.Helpers;
using OnlineClassScheduler.Views.Pages;

namespace OnlineClassScheduler.Views;

/// <summary>
/// An empty window that can be used on its own or navigated to within a Frame.
/// </summary>
public sealed partial class MainWindow : Window
{
	private const string OnlineClassesPage = nameof(Pages.OnlineClassesPage);
	private const string SettingsPage = nameof(Pages.SettingsPage);

	private static readonly Dictionary<string, Type> _pageMap = new()
	{
		[OnlineClassesPage] = typeof(OnlineClassesPage),
		[SettingsPage] = typeof(SettingsPage)
	};

	public MainWindow()
	{
		this.InitializeComponent();
		base.Title = "Online Class Scheduler";

		this.customTitleBarTextBlock.Text = base.Title;
		base.ExtendsContentIntoTitleBar = true;
		base.SetTitleBar(this.customTitleBar);

		var colorTheme = App.QueryCollection.GetAppSettings().ColorTheme;

		((FrameworkElement)base.Content).RequestedTheme = colorTheme;
		ThemeHelper.SetWindowCaptionTheme(
			colorTheme == ElementTheme.Light ? ApplicationTheme.Light : ApplicationTheme.Dark);
	}

	public void ShowUnsavedIndicator(bool showIndicator = true)
		=> this.customTitleBarTextBlock.Text = showIndicator ? '*' + base.Title : base.Title;



	/**********************************************************************
	 * NAVIGATIONVIEW
	 */
	private void NavigationViewOnLoaded(object sender, RoutedEventArgs e)
	{
		var viewItem = (NavigationViewItem)this.navigationView.MenuItems[0];
		this.navigationView.SelectedItem = viewItem;
		this.NavigateToPage(viewItem);
	}

	private void NavigationViewOnSelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
	{
		if (args.IsSettingsSelected)
		{
			this.navigationView.Header = "Settings";
			_ = this.navigationFrame.Navigate(_pageMap[SettingsPage]);
		}
		else
		{
			this.NavigateToPage((NavigationViewItem)args.SelectedItem);
		}
	}

	private void NavigateToPage(NavigationViewItem viewItem)
	{
		this.navigationView.Header = viewItem.Content;
		_ = this.navigationFrame.Navigate(_pageMap[viewItem.Tag.ToString()!]);
	}
}
