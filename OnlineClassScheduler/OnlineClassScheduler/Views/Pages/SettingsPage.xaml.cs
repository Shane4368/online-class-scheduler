﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using OnlineClassScheduler.Helpers;
using OnlineClassScheduler.ViewModels;

namespace OnlineClassScheduler.Views.Pages;

/// <summary>
/// An empty page that can be used on its own or navigated to within a Frame.
/// </summary>
public sealed partial class SettingsPage : Page
{
	private readonly SettingsViewModel ViewModel = new();

	public SettingsPage()
	{
		this.InitializeComponent();
	}

	private void RadioButtonsOnSelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		var radioButtons = (RadioButtons)sender;
		var selectedIndex = radioButtons.SelectedIndex;

		if (selectedIndex == -1) return;

		var selectedItem = (RadioButton)radioButtons.Items[selectedIndex];
		var frameworkElement = (FrameworkElement)App.MainWindow.Content;

		switch (selectedItem.Content)
		{
			case "Light":
				frameworkElement.RequestedTheme = ElementTheme.Light;
				ThemeHelper.SetWindowCaptionTheme(ApplicationTheme.Light);
				break;
			case "Dark":
				frameworkElement.RequestedTheme = ElementTheme.Dark;
				ThemeHelper.SetWindowCaptionTheme(ApplicationTheme.Dark);
				break;
			default:
				frameworkElement.RequestedTheme = ElementTheme.Default;
				ThemeHelper.SetWindowCaptionTheme(Application.Current.RequestedTheme);
				break;
		}
	}
}
