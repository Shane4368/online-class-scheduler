﻿using System.Collections.Generic;
using System.Linq;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Shapes;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;

namespace OnlineClassScheduler.Views.Pages;

/// <summary>
/// An empty window that can be used on its own or navigated to within a Frame.
/// </summary>
public sealed partial class OnlineClassesPage : Page
{
	private readonly OnlineClassesViewModel ViewModel = new();

	public OnlineClassesPage()
	{
		this.InitializeComponent();
	}



	/**********************************************************************
	 * DATAGRID
	 */
	private bool _dataGridColumnContextMenuRequested;
	private bool _dataGridColumnVisibilityChanged;

	private void DataGridOnLoading(FrameworkElement sender, object args)
	{
		void CreateDataGridColumnSettingsList()
		{
			var columnSettingsList = new List<DataGridColumnSettings>(this.dataGrid.Columns.Count);

			foreach (var column in this.dataGrid.Columns)
			{
				var columnSettings = new DataGridColumnSettings(column.Header.ToString()!)
				{
					DisplayIndex = column.DisplayIndex,
					Visibility = column.Visibility
				};

				columnSettingsList.Add(columnSettings);
			}

			this.ViewModel.SetDataGridColumnSettingsList(columnSettingsList.AsReadOnly());
			this.ViewModel.SaveDataGridColumnSettingsList(true);
		}

		void InitFromDataGridColumnSettingsList()
		{
			var columnSettingsList = this.ViewModel.DataGridColumnSettingsList;

			foreach (var columnSettings in columnSettingsList)
			{
				var column = this.dataGrid.Columns.First(x => x.Header.Equals(columnSettings.ColumnName));
				column.DisplayIndex = columnSettings.DisplayIndex;
				column.Visibility = columnSettings.Visibility;
			}
		}


		if (this.ViewModel.DataGridColumnSettingsList.Count == 0)
			CreateDataGridColumnSettingsList();
		else
			InitFromDataGridColumnSettingsList();
	}

	private void DataGridOnColumnHeaderDragCompleted(object sender, DragCompletedEventArgs e)
	{
		var columnSettingsList = this.ViewModel.DataGridColumnSettingsList;

		foreach (var column in this.dataGrid.Columns)
		{
			var columnSettings = columnSettingsList.First(x => x.ColumnName.Equals(column.Header));
			columnSettings.DisplayIndex = column.DisplayIndex;
		}

		this.ViewModel.SaveDataGridColumnSettingsList();
	}

	private void DataGridOnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
	{
		if (e.EditingElement is TextBox textBox && textBox.Text != null)
			textBox.Text = textBox.Text.Trim();
	}

	private void DataGridOnCellEditEnded(object sender, DataGridCellEditEndedEventArgs e)
	{
		var onlineClass = (OnlineClass)e.Row.DataContext;

		if (onlineClass.IsModified)
			((MainWindow)App.MainWindow).ShowUnsavedIndicator();
	}

	private void DataGridOnContextRequested(UIElement sender, ContextRequestedEventArgs args)
	{
		var originalSourceIsRectangle = args.OriginalSource is Rectangle;
		var frameworkElement = (FrameworkElement)args.OriginalSource;
		var hasPosition = args.TryGetPosition(sender, out var point);
		var flyoutOptions = new FlyoutShowOptions { Position = hasPosition ? point : null };

		if (originalSourceIsRectangle || frameworkElement.DataContext is string)
		{
			// column header
			this.SetupListenersDataGridContextMenuCheckBoxes();
			this.dataGridColumnContextMenu.ShowAt(sender, flyoutOptions);
		}
		else if (frameworkElement.DataContext is OnlineClass)
		{
			// cell
			this.dataGrid.SelectedItem = frameworkElement.DataContext;
			this.dataGridRowContextMenu.ShowAt(sender, flyoutOptions);
		}
	}



	private void DataGridColumnContextMenuOnClosed(object sender, object e)
	{
		if (!this._dataGridColumnVisibilityChanged) return;

		var columnSettingsList = this.ViewModel.DataGridColumnSettingsList;

		foreach (var column in this.dataGrid.Columns)
		{
			var columnSettings = columnSettingsList.First(x => x.ColumnName.Equals(column.Header));
			columnSettings.DisplayIndex = column.DisplayIndex;
			columnSettings.Visibility = column.Visibility;
		}

		this.ViewModel.SaveDataGridColumnSettingsList();
		this._dataGridColumnVisibilityChanged = false;
	}



	private void DataGridColumnContextMenuCheckBoxOnClick(object sender, RoutedEventArgs e)
	{
		var checkBoxSender = (CheckBox)sender;
		var isCheckBoxSenderChecked = checkBoxSender.IsChecked.GetValueOrDefault();
		var checkBoxes = this.dataGridColumnContextMenuCheckBoxes.Children;
		var visibleColumnCount = this.dataGrid.Columns.Count(x => x.Visibility == Visibility.Visible);
		var column = this.dataGrid.Columns.First(x => x.Header.Equals(checkBoxSender.Content));

		// ensure at least one column remains visible
		if (visibleColumnCount == 2 && !isCheckBoxSenderChecked)
		{
			var checkBox = (CheckBox)checkBoxes.First(x => !x.Equals(sender) && ((CheckBox)x).IsChecked.GetValueOrDefault());
			checkBox.IsEnabled = false;
		}
		else if (visibleColumnCount == 1)
		{
			var checkBox = (CheckBox)checkBoxes.First(x => !((CheckBox)x).IsEnabled);
			checkBox.IsEnabled = true;
		}

		// toggle column visibility
		if (!isCheckBoxSenderChecked)
			column.Visibility = Visibility.Collapsed;
		else
		{
			column.DisplayIndex = this.dataGrid.Columns.Count - 1;
			column.Visibility = Visibility.Visible;
		}

		this._dataGridColumnVisibilityChanged = true;
	}

	private void SetupListenersDataGridContextMenuCheckBoxes()
	{
		if (this._dataGridColumnContextMenuRequested) return;

		foreach (var child in this.dataGridColumnContextMenuCheckBoxes.Children)
		{
			var checkBox = (CheckBox)child;
			var column = this.dataGrid.Columns.First(x => x.Header.Equals(checkBox.Content));

			checkBox.IsChecked = column.Visibility == Visibility.Visible;
			checkBox.Click += this.DataGridColumnContextMenuCheckBoxOnClick;
		}

		this._dataGridColumnContextMenuRequested = true;
	}
}
