﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Input;
using OnlineClassScheduler.Controls;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;

namespace OnlineClassScheduler.Views.Dialogs;

public sealed partial class OnlineClassModalDialog : ContentDialog
{
	private readonly OnlineClassModalDialogViewModel ViewModel = null!;

	private bool _timePickerSelectedTimeIsNull = true;

	private OnlineClassModalDialog()
	{
		this.InitializeComponent();

		// always set XamlRoot if not in Xaml code
		base.XamlRoot = App.MainWindow.Content.XamlRoot;
	}

	public OnlineClassModalDialog(OnlineClass onlineClass) : this()
	{
		this.ViewModel = new OnlineClassModalDialogViewModel(onlineClass);

		this.timePicker.ClockIdentifier = this.ViewModel.AppSettings.UseMilitaryTime
			? "24HourClock" : "12HourClock";
	}

	public bool UseEditMode { get; set; }



	public new async Task<ContentDialogResult> ShowAsync()
	{
		if (!this.UseEditMode)
		{
			this.timePicker.SelectedTime = null; // prevents defaulting to 12AM
			return await base.ShowAsync();
		}

		return await this.ShowEditAsync();
	}

	private async Task<ContentDialogResult> ShowEditAsync()
	{
		this.timePicker.GettingFocus += this.TimePickerOnGettingFocus;

		var result = await base.ShowAsync();

		this.timePicker.GettingFocus -= this.TimePickerOnGettingFocus;

		return !this.ViewModel.OnlineClass.IsModified
			? ContentDialogResult.None
			: result;
	}

	private void TimePickerOnGettingFocus(UIElement sender, GettingFocusEventArgs args)
	{
		if (!this._timePickerSelectedTimeIsNull) return;

		// temporary fix to show initial time
		this.timePicker.SelectedTime = TimeSpan.Parse(this.ViewModel.OnlineClass.Time!);
		this._timePickerSelectedTimeIsNull = false;
	}

	private void OnPrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
	{
		var hasError = false;

		foreach (var control in this.stackPanel.Children.OfType<IErrorControl>())
		{
			control.CheckError();

			if (control.HasError) hasError = true;
		}

		if (hasError) args.Cancel = true;
	}
}
