﻿using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace OnlineClassScheduler.Controls;

public sealed class RequirableComboBox : Control, IErrorControl
{
	public RequirableComboBox()
	{
		base.DefaultStyleKey = typeof(RequirableComboBox);
	}

	public static readonly DependencyProperty HeaderProperty
		= DependencyProperty.Register(nameof(Header), typeof(string), typeof(RequirableComboBox), new PropertyMetadata(null));

	public static readonly DependencyProperty ItemsSourceProperty
		= DependencyProperty.Register(nameof(ItemsSource), typeof(string[]), typeof(RequirableComboBox), new PropertyMetadata(null));

	public static readonly DependencyProperty SelectedItemProperty
		= DependencyProperty.Register(nameof(SelectedItem), typeof(string), typeof(RequirableComboBox), new PropertyMetadata(null));

	public string Header
	{
		get => (string)base.GetValue(HeaderProperty);
		set => base.SetValue(HeaderProperty, value);
	}

	public string[] ItemsSource
	{
		get => (string[])base.GetValue(ItemsSourceProperty);
		set => base.SetValue(ItemsSourceProperty, value);
	}

	public string SelectedItem
	{
		get => (string)base.GetValue(SelectedItemProperty);
		set => base.SetValue(SelectedItemProperty, value);
	}

	public bool IsRequired { get; set; }
	public bool HasError { get; private set; }

	public void CheckError() => this.ComboBoxOnLostFocus(null, null);



	private ComboBox? _comboBox;
	private TextBlock? _textBlock;
	private VisualState? _visualState;

	/**********************************************************************
	 * OVERRIDES
	 */
	protected override void OnApplyTemplate()
	{
		if (this._comboBox != null)
		{
			this._comboBox.LostFocus -= this.ComboBoxOnLostFocus;
			this._comboBox.SelectionChanged -= this.ComboBoxOnSelectionChanged;

			this.ShowError(null);
		}

		this._comboBox = (ComboBox)base.GetTemplateChild("comboBox");
		this._textBlock = (TextBlock)base.GetTemplateChild("textBlock");
		this._visualState = (VisualState)base.GetTemplateChild("InvalidInputVisualState");

		this._comboBox.LostFocus += this.ComboBoxOnLostFocus;
		this._comboBox.SelectionChanged += this.ComboBoxOnSelectionChanged;
	}



	/**********************************************************************
	 * EVENT HANDLERS
	 */
	private void ComboBoxOnLostFocus(object? sender, RoutedEventArgs? e)
	{
		if (this.IsRequired && string.IsNullOrEmpty(this._comboBox!.SelectedValue?.ToString()))
			this.ShowError("Field value must be selected");
		else
			this.ShowError(null);
	}

	private void ComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		this.ShowError(null);
	}

	private void ShowError(string? text)
	{
		var stateTrigger = (StateTrigger)this._visualState!.StateTriggers[0];

		if (text == null)
		{
			stateTrigger.IsActive = false;
			this.HasError = false;
		}
		else
		{
			stateTrigger.IsActive = true;
			this.HasError = true;
		}

		this._textBlock!.Text = text;
	}
}
