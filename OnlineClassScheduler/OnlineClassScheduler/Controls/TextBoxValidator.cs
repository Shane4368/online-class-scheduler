﻿using System;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Input;

namespace OnlineClassScheduler.Controls;

public sealed class TextBoxValidator : Control, IErrorControl
{
	public TextBoxValidator()
	{
		base.DefaultStyleKey = typeof(TextBoxValidator);
	}

	public static readonly DependencyProperty TextProperty
		= DependencyProperty.Register(nameof(Text), typeof(string), typeof(TextBoxValidator), new PropertyMetadata(null));

	public static readonly DependencyProperty HeaderProperty
		= DependencyProperty.Register(nameof(Header), typeof(string), typeof(TextBoxValidator), new PropertyMetadata(null));

	public string Text
	{
		get => (string)base.GetValue(TextProperty);
		set => base.SetValue(TextProperty, value);
	}

	public string Header
	{
		get => (string)base.GetValue(HeaderProperty);
		set => base.SetValue(HeaderProperty, value);
	}

	public bool IsRequired { get; set; }
	public InputScopeNameValue InputScope { get; set; }
	public bool HasError { get; private set; }

	public void CheckError() => this.TextBoxOnLostFocus(null, null);



	private TextBox? _textBox;
	private TextBlock? _textBlock;
	private VisualState? _visualState;

	/**********************************************************************
	 * OVERRIDES
	 */
	protected override void OnApplyTemplate()
	{
		if (this._textBox != null)
		{
			this._textBox.LostFocus -= this.TextBoxOnLostFocus;
			this.ShowError(null);
		}

		this._textBox = (TextBox)base.GetTemplateChild("textBox");
		this._textBlock = (TextBlock)base.GetTemplateChild("textBlock");
		this._visualState = (VisualState)base.GetTemplateChild("InvalidInputVisualState");

		this._textBox.LostFocus += this.TextBoxOnLostFocus;
	}



	/**********************************************************************
	 * EVENT HANDLERS
	 */
	private void TextBoxOnLostFocus(object? sender, RoutedEventArgs? e)
	{
		this._textBox!.Text = this._textBox.Text.Trim();

		if (this.IsRequired)
		{
			this.ShowError(
				string.IsNullOrEmpty(this._textBox.Text)
				? "Field value is required"
				: this.ValidateInputScope());

			return;
		}

		this.ShowError(this.ValidateInputScope());
	}

	private string? ValidateInputScope()
	{
		if (this.InputScope == InputScopeNameValue.Url && !Uri.IsWellFormedUriString(this._textBox!.Text, UriKind.Absolute))
			return "Field value is not a valid URL";

		return null;
	}

	private void ShowError(string? text)
	{
		var stateTrigger = (StateTrigger)this._visualState!.StateTriggers[0];

		if (text == null)
		{
			stateTrigger.IsActive = false;
			this.HasError = false;
		}
		else
		{
			stateTrigger.IsActive = true;
			this.HasError = true;
		}

		this._textBlock!.Text = text;
	}
}
