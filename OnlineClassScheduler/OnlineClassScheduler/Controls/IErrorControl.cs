﻿namespace OnlineClassScheduler.Controls;

public interface IErrorControl
{
	public bool HasError { get; }

	public void CheckError();
}
