﻿using System;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;

namespace OnlineClassScheduler.Controls;

public sealed class RequirableTimePicker : Control, IErrorControl
{
	public RequirableTimePicker()
	{
		base.DefaultStyleKey = typeof(RequirableTimePicker);
	}

	public static readonly DependencyProperty HeaderProperty
		= DependencyProperty.Register(nameof(Header), typeof(string), typeof(RequirableTimePicker), new PropertyMetadata(null));

	public static readonly DependencyProperty ClockIdentifierProperty
		= DependencyProperty.Register(nameof(ClockIdentifier), typeof(string), typeof(RequirableTimePicker), new PropertyMetadata(null));

	public static readonly DependencyProperty SelectedTimeProperty
		= DependencyProperty.Register(nameof(SelectedTime), typeof(TimeSpan?), typeof(RequirableTimePicker), new PropertyMetadata(null));

	public string Header
	{
		get => (string)base.GetValue(HeaderProperty);
		set => base.SetValue(HeaderProperty, value);
	}

	public string ClockIdentifier
	{
		get => (string)base.GetValue(ClockIdentifierProperty);
		set => base.SetValue(ClockIdentifierProperty, value);
	}

	public TimeSpan? SelectedTime
	{
		get => (TimeSpan?)base.GetValue(SelectedTimeProperty);
		set => base.SetValue(SelectedTimeProperty, value);
	}

	public bool IsRequired { get; set; }
	public bool HasError { get; private set; }

	public void CheckError() => this.TimePickerOnLostFocus(null, null);



	private TimePicker? _timePicker;
	private TextBlock? _textBlock;
	private VisualState? _visualState;

	/**********************************************************************
	 * OVERRIDES
	 */
	protected override void OnApplyTemplate()
	{
		if (this._timePicker != null)
		{
			this._timePicker.LostFocus -= this.TimePickerOnLostFocus;
			this._timePicker.SelectedTimeChanged -= this.TimePickerOnSelectedTimeChanged;

			this.ShowError(null);
		}

		this._timePicker = (TimePicker)base.GetTemplateChild("timePicker");
		this._textBlock = (TextBlock)base.GetTemplateChild("textBlock");
		this._visualState = (VisualState)base.GetTemplateChild("InvalidInputVisualState");

		this._timePicker.LostFocus += this.TimePickerOnLostFocus;
		this._timePicker.SelectedTimeChanged += this.TimePickerOnSelectedTimeChanged;
	}



	/**********************************************************************
	 * EVENT HANDLERS
	 */
	private void TimePickerOnLostFocus(object? sender, RoutedEventArgs? e)
	{
		if (this.IsRequired && this._timePicker!.SelectedTime == null)
			this.ShowError("Time must be set");
		else
			this.ShowError(null);
	}

	private void TimePickerOnSelectedTimeChanged(TimePicker sender, TimePickerSelectedValueChangedEventArgs args)
	{
		this.ShowError(null);
	}

	private void ShowError(string? text)
	{
		var stateTrigger = (StateTrigger)this._visualState!.StateTriggers[0];

		if (text == null)
		{
			stateTrigger.IsActive = false;
			this.HasError = false;
		}
		else
		{
			stateTrigger.IsActive = true;
			this.HasError = true;
		}

		this._textBlock!.Text = text;
	}
}
