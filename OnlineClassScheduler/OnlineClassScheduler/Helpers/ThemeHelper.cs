﻿using Microsoft.UI.Xaml;
using Windows.UI;

namespace OnlineClassScheduler.Helpers;

public static class ThemeHelper
{
	public static void SetWindowCaptionTheme(ApplicationTheme theme)
	{
		var resources = Application.Current.Resources;

		if (theme == ApplicationTheme.Light)
		{
			resources["WindowCaptionButtonBackgroundPointerOver"] = Color.FromArgb(255, 234, 234, 234);
			resources["WindowCaptionButtonBackgroundPressed"] = Color.FromArgb(255, 209, 209, 209);
			resources["WindowCaptionForeground"] = Color.FromArgb(255, 0, 0, 0);
			resources["WindowCaptionForegroundDisabled"] = Color.FromArgb(255, 32, 32, 32);
		}
		else
		{
			resources["WindowCaptionButtonBackgroundPointerOver"] = Color.FromArgb(255, 45, 45, 45);
			resources["WindowCaptionButtonBackgroundPressed"] = Color.FromArgb(255, 62, 62, 62);
			resources["WindowCaptionForeground"] = Color.FromArgb(255, 255, 255, 255);
			resources["WindowCaptionForegroundDisabled"] = Color.FromArgb(255, 169, 169, 169);
		}
	}
}
