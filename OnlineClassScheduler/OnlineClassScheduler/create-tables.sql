﻿CREATE TABLE IF NOT EXISTS online_class (
	id TEXT PRIMARY KEY,
	occurrence VARCHAR(3),
	module_code VARCHAR(8), -- assuming all universities don't exceed this limit
	module_name TEXT NOT NULL,
	lecturer TEXT,
	[type] TEXT,
	[day] TEXT NOT NULL,
	[time] TEXT NOT NULL,
	uri TEXT NOT NULL,
	minutes_ahead INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS datagrid_column (
	[name] TEXT PRIMARY KEY,
	visibility INTEGER NOT NULL,
	display_index INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS app (
	id TEXT PRIMARY KEY,
	use_military_time INTEGER NOT NULL,
	theme INTEGER NOT NULL,
	minutes_ahead INTEGER NOT NULL
);