﻿using Microsoft.Data.Sqlite;
using Microsoft.UI.Xaml;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Extensions;

public static class SqliteDataReaderExtensions
{
	public static string? GetSafeString(this SqliteDataReader reader, int ordinal)
		=> !reader.IsDBNull(ordinal) ? reader.GetString(ordinal) : null;

	public static OnlineClass GetOnlineClass(this SqliteDataReader reader)
	{
		return new(reader.GetString(0))
		{
			Occurrence = reader.GetSafeString(1),
			ModuleCode = reader.GetSafeString(2),
			ModuleName = reader.GetString(3),
			LecturerName = reader.GetSafeString(4),
			Type = reader.GetSafeString(5),
			Day = reader.GetString(6),
			Time = reader.GetString(7),
			Uri = reader.GetString(8),
			MinutesAhead = reader.GetInt32(9),
			IsModified = false
		};
	}

	public static AppSettings GetAppSettings(this SqliteDataReader reader)
	{
		return new()
		{
			UseMilitaryTime = reader.GetBoolean(1),
			ColorTheme = (ElementTheme)reader.GetInt32(2),
			MinutesAhead = reader.GetInt32(3)
		};
	}

	public static DataGridColumnSettings GetDataGridColumnSettings(this SqliteDataReader reader)
	{
		return new(reader.GetString(0))
		{
			Visibility = (Visibility)reader.GetInt32(1),
			DisplayIndex = reader.GetInt32(2)
		};
	}
}
