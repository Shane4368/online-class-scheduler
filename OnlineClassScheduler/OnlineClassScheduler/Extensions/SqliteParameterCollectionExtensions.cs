﻿using System;
using Microsoft.Data.Sqlite;

namespace OnlineClassScheduler.Extensions;

public static class SqliteParameterCollectionExtensions
{
	public static SqliteParameter AddWithSafeValue(this SqliteParameterCollection collection, string parameterName, object? value)
		=> collection.AddWithValue(parameterName, value ?? DBNull.Value);
}
