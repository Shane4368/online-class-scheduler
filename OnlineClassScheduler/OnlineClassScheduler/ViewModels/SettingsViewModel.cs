﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.UI.Xaml;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.ViewModels;

public sealed class SettingsViewModel : ObservableObject
{
	public SettingsViewModel()
	{
		this._colorThemeIndex = (int)this.AppSettings.ColorTheme;
	}

	public AppSettings AppSettings => App.QueryCollection.GetAppSettings();

	public int ColorThemeIndex
	{
		get => this._colorThemeIndex == 0 ? 2 : this._colorThemeIndex - 1;
		set => this.SetProperty(ref this._colorThemeIndex, value == 2 ? 0 : value + 1);
	}

	private int _colorThemeIndex;



	/**********************************************************************
	 * OVERRIDES
	 */
	protected override void SetProperty<T>(ref T property, in T value, [CallerMemberName] in string propertyName = null!)
	{
		if (object.Equals(value, property)) return;

		property = value;
		this.AppSettings.ColorTheme = Enum.Parse<ElementTheme>(value!.ToString()!);
		base.OnPropertyChanged(propertyName);
	}
}
