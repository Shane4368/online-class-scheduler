﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using OnlineClassScheduler.Commands;
using OnlineClassScheduler.Commands.Standard;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.Services;

namespace OnlineClassScheduler.ViewModels;

public sealed class OnlineClassesViewModel
{
	public OnlineClassesViewModel()
	{
		this.CopyCommand = new CopyCommand().Get();
		this.DeleteCommand = new DeleteCommand(this).Get();
		this.OpenCommand = new OpenCommand().Get();
		this.SaveCommand = new SaveCommand(this).Get();

		this.AddCommand = new AddCommand(this);
		this.DeleteAllCommand = new DeleteAllCommand(this);
		this.ImportCommand = new ImportCommand(this);
		this.ExportCsvCommand = new ExportCsvCommand(this);
		this.ExportIcsCommand = new ExportIcsCommand(this);

		this.EditCommand = new EditCommand(this);
		this.DuplicateCommand = new DuplicateCommand(this);
		this.CopyCompactCommand = new CopyCompactCommand();
	}

	public ICommand AddCommand { get; }
	public ICommand DeleteCommand { get; }
	public ICommand DeleteAllCommand { get; }
	public ICommand SaveCommand { get; }
	public ICommand ImportCommand { get; }
	public ICommand ExportIcsCommand { get; }
	public ICommand ExportCsvCommand { get; }

	public ICommand EditCommand { get; }
	public ICommand DuplicateCommand { get; }
	public ICommand CopyCommand { get; }
	public ICommand CopyCompactCommand { get; }
	public ICommand OpenCommand { get; }

	public string[] DaysOfWeek => Enum.GetNames(typeof(DayOfWeek));
	public string[] OnlineClassTypes => Enum.GetNames(typeof(OnlineClassType));
	public string CurrentSemester => Semester.GetCurrentSemester();

	public AppSettings AppSettings => App.QueryCollection.GetAppSettings();

	public ObservableCollection<OnlineClass> OnlineClasses
		=> App.QueryCollection.GetOnlineClasses();

	public IReadOnlyCollection<DataGridColumnSettings> DataGridColumnSettingsList
		=> App.QueryCollection.GetDataGridColumnSettingsList();



	/**********************************************************************
	 * DATAGRIDCOLUMNSETTINGS
	 */
	public void SaveDataGridColumnSettingsList(bool createNewRecords = false)
		=> App.QueryCollection.SaveDataGridColumnSettingsList(createNewRecords);

	public void SetDataGridColumnSettingsList(IReadOnlyCollection<DataGridColumnSettings> collection)
		=> App.QueryCollection.SetDataGridColumnSettingsList(collection);



	/**********************************************************************
	 * ONLINECLASS
	 */
	public void AddOnlineClass(OnlineClass onlineClass)
		=> App.QueryCollection.AddOnlineClass(onlineClass);

	public int RemoveOnlineClasses(IEnumerable<OnlineClass> onlineClasses)
		=> App.QueryCollection.RemoveOnlineClasses(onlineClasses);

	public int RemoveOnlineClasses()
		=> App.QueryCollection.RemoveOnlineClasses();

	public void SaveOnlineClasses()
	{
		App.QueryCollection.SaveOnlineClasses();
		BackgroundService.EnsureCreatedTask();
	}
}
