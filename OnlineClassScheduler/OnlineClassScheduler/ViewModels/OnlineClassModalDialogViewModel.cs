﻿using System;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.ViewModels;

public sealed class OnlineClassModalDialogViewModel
{
	public string[] DaysOfWeek => Enum.GetNames(typeof(DayOfWeek));

	public string[] OnlineClassTypes => Enum.GetNames(typeof(OnlineClassType));

	public AppSettings AppSettings => App.QueryCollection.GetAppSettings();

	public OnlineClass OnlineClass { get; }

	public OnlineClassModalDialogViewModel(OnlineClass onlineClass)
		=> this.OnlineClass = onlineClass;
}
