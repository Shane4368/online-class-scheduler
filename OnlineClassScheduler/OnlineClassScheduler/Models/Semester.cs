﻿using System;

namespace OnlineClassScheduler.Models;

public static class Semester
{
	private const string One = "Semester 1";
	private const string Two = "Semester 2";
	private const string Three = "Semester 3";

	public static string GetCurrentSemester()
	{
		var date = DateTime.Now;
		var month = date.Month;
		string semester;

		if (month >= 8) semester = One;
		else if (month >= 5) semester = Three;
		else semester = Two;

		return $"{date.Year} {semester}";
	}
}
