﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace OnlineClassScheduler.Models;

// https://stackoverflow.com/questions/1315621/implementing-inotifypropertychanged-does-a-better-way-exist

public abstract class ObservableObject : INotifyPropertyChanged
{
	public event PropertyChangedEventHandler? PropertyChanged;

	protected virtual void SetProperty<T>(ref T property, in T value, [CallerMemberName] in string propertyName = null!)
	{
		if (!object.Equals(value, property))
		{
			property = value;
			this.OnPropertyChanged(propertyName);
		}
	}

	protected void OnPropertyChanged(in string propertyName)
		=> this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}
