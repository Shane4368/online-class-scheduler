﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CsvHelper.Configuration.Attributes;
using Microsoft.Toolkit.Uwp.Notifications;
using OnlineClassScheduler.Configuration;

namespace OnlineClassScheduler.Models;

public sealed class OnlineClass : ObservableObject, INotifyDataErrorInfo, ICloneable
{
	public OnlineClass() => this.Id = Guid.NewGuid().ToString();

	public OnlineClass(string id) => this.Id = id;

	public OnlineClass(OnlineClass onlineClass)
	{
		this.Id = onlineClass.Id;
		this.Occurrence = onlineClass.Occurrence;
		this.ModuleCode = onlineClass.ModuleCode;
		this.ModuleName = onlineClass.ModuleName;
		this.LecturerName = onlineClass.LecturerName;
		this.Type = onlineClass.Type;
		this.Day = onlineClass.Day;
		this.Time = onlineClass.Time;
		this.Uri = onlineClass.Uri;
		this.MinutesAhead = onlineClass.MinutesAhead;
	}


	[Ignore]
	public string Id { get; }

	public string? Occurrence
	{
		get => this._occurrence;
		set => this.SetProperty(ref this._occurrence, value);
	}

	public string? ModuleCode
	{
		get => this._moduleCode;
		set => this.SetProperty(ref this._moduleCode, value);
	}

	public string? ModuleName
	{
		get => this._moduleName;
		set
		{
			if (!string.IsNullOrWhiteSpace(value))
				this.SetProperty(ref this._moduleName, value);
		}
	}

	public string? LecturerName
	{
		get => this._lecturerName;
		set => this.SetProperty(ref this._lecturerName, value);
	}

	public string? Type
	{
		get => this._type;
		set => this.SetProperty(ref this._type, value);
	}

	public string? Day
	{
		get => this._day;
		set => this.SetProperty(ref this._day, value);
	}

	public string? Time
	{
		get => this._time;
		set
		{
			if (!string.IsNullOrWhiteSpace(value))
				this.SetProperty(ref this._time, value);
		}
	}

	public string Uri
	{
		get => this._uri;
		set
		{
			if (string.IsNullOrWhiteSpace(value)) return;

			this.SetProperty(ref this._uri, value);

			if (!System.Uri.IsWellFormedUriString(value, UriKind.Absolute))
				this.AddOrRemoveError("Invalid URI");
			else
				this.AddOrRemoveError(null, false);
		}
	}

	public int MinutesAhead
	{
		get => this._minutesAhead;
		set => this.SetProperty(ref this._minutesAhead, value);
	}

	private string? _occurrence;
	private string? _moduleCode;
	private string? _moduleName;
	private string? _lecturerName;
	private string? _type;
	private string? _day;
	private string? _time;
	private string _uri = AppConfiguration.DefaultOnlineClassUrl;
	private int _minutesAhead;

	public object Clone()
	{
		return new OnlineClass
		{
			Occurrence = this._occurrence,
			ModuleCode = this._moduleCode,
			ModuleName = this._moduleName,
			LecturerName = this._lecturerName,
			Type = this._type,
			Day = this._day,
			Time = this._time,
			Uri = this._uri,
			MinutesAhead = this._minutesAhead
		};
	}

	public string ToCompactString()
	{
		var text = string.Empty;

		if (!string.IsNullOrEmpty(this._lecturerName))
			text = $"Lecturer: {this._lecturerName}\n";

		text += $"{this._occurrence} {this._moduleCode} {this._moduleName} {this._type} {this._day}@{this._time}\n";
		text += this._uri;

		return string.Join(' ', text.Split(' ', StringSplitOptions.RemoveEmptyEntries));
	}



	/**********************************************************************
	 * TOAST NOTIFICATION
	 */
	public void ShowToastNotification()
	{
		var comboBoxItems = new (string, string)[]
		{
			("5", "5 minutes"),
			("10", "10 minutes"),
			("15", "15 minutes"),
			("20", "20 minutes"),
			("25", "25 minutes")
		};

		new ToastContentBuilder()
			.AddHeader(nameof(OnlineClass), this._moduleName, string.Empty)
			.AddText($"{this._occurrence} {this._type} {this._lecturerName}")
			.AddComboBox("snoozeFor", "Snooze for", comboBoxItems[0].Item1, comboBoxItems)
			.AddButton(new ToastButtonSnooze { SelectionBoxId = "snoozeFor" })
			.AddButton(new ToastButtonDismiss())
			.AddButton("Go to class", ToastActivationType.Protocol, this._uri)
			.Show();
	}

	public DateTime GetTriggerStartBoundary()
	{
		var timeSpan = TimeSpan.Parse(this._time!);
		var dateTimeToday = DateTime.Today;
		var currentDay = dateTimeToday.DayOfWeek;
		var startDay = Enum.Parse<DayOfWeek>(this._day!);
		var difference = startDay - currentDay;
		DateTime start;

		if (difference > 0)
			start = dateTimeToday.AddDays(difference);
		else if (difference < 0)
			start = dateTimeToday.AddDays(difference + 7);
		else
			start = dateTimeToday;

		return start.Add(timeSpan);
	}


	[Ignore]
	public bool IsModified { get; set; }

	/**********************************************************************
	 * OVERRIDES
	 */
	protected override void SetProperty<T>(ref T property, in T value, [CallerMemberName] in string propertyName = null!)
	{
		if (object.Equals(value, property)) return;

		property = value;
		this.IsModified = true;

		base.OnPropertyChanged(propertyName);
	}

	public override string ToString()
	{
		var values = new List<string>(8);

		if (!string.IsNullOrEmpty(this.Occurrence))
			values.Add($"Occurrence: {this.Occurrence}");

		if (!string.IsNullOrEmpty(this.ModuleCode))
			values.Add($"Module Code: {this.ModuleCode}");

		values.Add($"Module Name: {this.ModuleName}");

		if (!string.IsNullOrEmpty(this.LecturerName))
			values.Add($"Lecturer Name: {this.LecturerName}");

		if (!string.IsNullOrEmpty(this.Type))
			values.Add($"Type: {this.Type}");

		values.Add($"Day: {this.Day}");
		values.Add($"Time: {this.Time}");
		values.Add($"URI: <{this.Uri}>");

		return string.Join('\n', values);
	}



	/**********************************************************************
	 * INOTIFYDATAERRORINFO IMPLEMENTATION
	 */
	private readonly Dictionary<string, string> _errorByProperty = new();

	private void AddOrRemoveError(string? error, bool hasError = true, [CallerMemberName] string propertyName = null!)
	{
		this.HasErrors = hasError;
		var hasKey = this._errorByProperty.ContainsKey(propertyName);

		if (hasError && !hasKey)
		{
			this._errorByProperty.Add(propertyName, error!);
			this.OnErrorsChanged(propertyName);
		}
		else if (!hasError && hasKey)
		{
			_ = this._errorByProperty.Remove(propertyName);
			this.OnErrorsChanged(propertyName);
		}
	}

	private void OnErrorsChanged(in string propertyName)
		=> this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

	public IEnumerable GetErrors(string? propertyName)
	{
		return !string.IsNullOrEmpty(propertyName)
			? new[] { this._errorByProperty[propertyName] }
			: this._errorByProperty.Values;
	}

	public event EventHandler<DataErrorsChangedEventArgs>? ErrorsChanged;

	[Ignore]
	public bool HasErrors { get; private set; }
}
