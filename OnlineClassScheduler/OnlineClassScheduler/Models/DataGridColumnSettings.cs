﻿using Microsoft.UI.Xaml;

namespace OnlineClassScheduler.Models;

public sealed class DataGridColumnSettings
{
	public DataGridColumnSettings(string columnName) => this.ColumnName = columnName;

	public string ColumnName { get; }
	public Visibility Visibility { get; set; }
	public int DisplayIndex { get; set; }
}
