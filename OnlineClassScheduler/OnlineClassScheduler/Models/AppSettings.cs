﻿using Microsoft.UI.Xaml;

namespace OnlineClassScheduler.Models;

public sealed class AppSettings : ObservableObject
{
	public bool UseMilitaryTime
	{
		get => this._useMilitaryTime;
		set => base.SetProperty(ref this._useMilitaryTime, value);
	}

	public ElementTheme ColorTheme
	{
		get => this._colorTheme;
		set => base.SetProperty(ref this._colorTheme, value);
	}

	public int MinutesAhead
	{
		get => this._minutesAhead;
		set => base.SetProperty(ref this._minutesAhead, value);
	}

	private bool _useMilitaryTime;
	private ElementTheme _colorTheme = ElementTheme.Default;
	private int _minutesAhead;
}
