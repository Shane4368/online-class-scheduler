﻿namespace OnlineClassScheduler.Models;

public enum OnlineClassType
{
	Lecture,
	Tutorial,
	Lab
}
