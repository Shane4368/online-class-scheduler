﻿using System;
using System.Windows.Input;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;

namespace OnlineClassScheduler.Commands;

public sealed class DeleteAllCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public DeleteAllCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		return this._viewModel.OnlineClasses != null;
	}

	public void Execute(object? parameter)
	{
		var count = this._viewModel.RemoveOnlineClasses();
		((MainWindow)App.MainWindow).ShowUnsavedIndicator(count > 0);
	}
}
