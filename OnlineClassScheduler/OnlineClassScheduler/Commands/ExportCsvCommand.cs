﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Input;
using CsvHelper;
using OnlineClassScheduler.ViewModels;
using Windows.Storage;
using Windows.Storage.Pickers;
using WinRT.Interop;

namespace OnlineClassScheduler.Commands;

public sealed class ExportCsvCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public ExportCsvCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		return this._viewModel.OnlineClasses != null;
	}

	public async void Execute(object? parameter)
	{
		var windowHandle = WindowNative.GetWindowHandle(App.MainWindow);
		var filePicker = new FileSavePicker();

		InitializeWithWindow.Initialize(filePicker, windowHandle);

		filePicker.FileTypeChoices.Add("Comma Separated Values", new[] { ".csv" });
		filePicker.SuggestedFileName = "timetable.csv";

		var file = await filePicker.PickSaveFileAsync();

		if (file == null) return;

		using var writer = new StreamWriter(file.Path);
		using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);

		CachedFileManager.DeferUpdates(file);
		csv.WriteRecords(this._viewModel.OnlineClasses);
		_ = await CachedFileManager.CompleteUpdatesAsync(file);
	}
}
