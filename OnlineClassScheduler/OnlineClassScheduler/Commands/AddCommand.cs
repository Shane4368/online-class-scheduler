﻿using System;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Controls;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;
using OnlineClassScheduler.Views.Dialogs;

namespace OnlineClassScheduler.Commands;

public sealed class AddCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public AddCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		return this._viewModel.OnlineClasses != null;
	}

	public async void Execute(object? parameter)
	{
		var dataGrid = (DataGrid)parameter!;
		var onlineClass = new OnlineClass();

		var dialog = new OnlineClassModalDialog(onlineClass)
		{
			Title = "Add Online Class",
			PrimaryButtonText = "Add class"
		};

		var result = await dialog.ShowAsync();

		if (result != ContentDialogResult.Primary) return;

		this._viewModel.AddOnlineClass(onlineClass);

		dataGrid.SelectedItem = onlineClass;
		dataGrid.ScrollIntoView(onlineClass, null);

		((MainWindow)App.MainWindow).ShowUnsavedIndicator();
	}
}
