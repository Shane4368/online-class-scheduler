﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using CsvHelper;
using CsvHelper.Configuration;
using Ical.Net;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;
using Windows.Storage;
using Windows.Storage.Pickers;
using WinRT.Interop;

namespace OnlineClassScheduler.Commands;

public sealed class ImportCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public ImportCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		return this._viewModel.OnlineClasses != null;
	}

	public async void Execute(object? parameter)
	{
		var windowHandle = WindowNative.GetWindowHandle(App.MainWindow);
		var filePicker = new FileOpenPicker();

		InitializeWithWindow.Initialize(filePicker, windowHandle);
		filePicker.FileTypeFilter.Add(".ics");
		filePicker.FileTypeFilter.Add(".csv");

		var file = await filePicker.PickSingleFileAsync();

		if (file == null) return;

		if (file.Path.EndsWith(".ics"))
			this.ReadIcsFile(file);
		else if (file.Path.EndsWith(".csv"))
			this.ReadCsvFile(file);

		((MainWindow)App.MainWindow).ShowUnsavedIndicator();
	}

	private void ReadIcsFile(StorageFile file)
	{
		var calendar = Ical.Net.Calendar.Load(File.ReadAllText(file.Path));

		foreach (var calendarEvent in calendar.Events)
		{
			var onlineClass = new OnlineClass();
			var dtStart = calendarEvent.DtStart;
			var alarm = calendarEvent.Alarms.FirstOrDefault(x => x.Group == Components.Alarm);
			int minutesAhead = 0;

			if (alarm != null)
			{
				var duration = alarm.Trigger.Duration;

				if (duration.HasValue)
					minutesAhead = Math.Abs((int)duration.Value.TotalMinutes);

				if (minutesAhead == this._viewModel.AppSettings.MinutesAhead)
					minutesAhead = 0;
			}

			onlineClass.Day = calendarEvent.DtStart.DayOfWeek.ToString();
			onlineClass.Time = new TimeOnly(dtStart.Hour, dtStart.Minute).ToString("H:mm");
			onlineClass.ModuleName = calendarEvent.Summary;
			onlineClass.MinutesAhead = minutesAhead;

			var match = new Regex("<a href=\"([^\"]+)\"").Match(calendarEvent.Description);

			if (match.Success)
				onlineClass.Uri = string.Join(null, match.Groups[1].Value.Where(x => !char.IsWhiteSpace(x)));

			this._viewModel.AddOnlineClass(onlineClass);
		}
	}

	private void ReadCsvFile(StorageFile file)
	{
		using var reader = new StreamReader(file.Path);
		using var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)
		{
			MissingFieldFound = null
		});

		_ = csv.Read();
		_ = csv.ReadHeader();

		while (csv.Read())
		{
			var onlineClass = csv.GetRecord<OnlineClass>()!;
			this._viewModel.AddOnlineClass(onlineClass);
		}
	}
}
