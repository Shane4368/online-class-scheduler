﻿using System;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;

namespace OnlineClassScheduler.Commands;

public sealed class DuplicateCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public DuplicateCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		if (parameter == null) return true;

		var dataGrid = (DataGrid)parameter;
		return dataGrid.SelectedItem != null;
	}

	public void Execute(object? parameter)
	{
		var dataGrid = (DataGrid)parameter!;
		var original = (OnlineClass)dataGrid.SelectedItem;
		var duplicate = (OnlineClass)original.Clone();

		this._viewModel.AddOnlineClass(duplicate);

		dataGrid.SelectedItem = duplicate;
		dataGrid.ScrollIntoView(duplicate, null);

		((MainWindow)App.MainWindow).ShowUnsavedIndicator();
	}
}
