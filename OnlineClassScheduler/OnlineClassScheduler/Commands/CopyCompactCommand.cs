﻿using System;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Controls;
using OnlineClassScheduler.Models;

namespace OnlineClassScheduler.Commands;

public sealed class CopyCompactCommand : ICommand
{
	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		if (parameter == null) return true;

		var dataGrid = (DataGrid)parameter;
		return dataGrid.SelectedItem != null;
	}

	public void Execute(object? parameter)
	{
		var dataGrid = (DataGrid)parameter!;

		var textBlock = new TextBlock
		{
			Text = ((OnlineClass)dataGrid.SelectedItem).ToCompactString(),
			IsTextSelectionEnabled = true
		};

		textBlock.SelectAll();
		textBlock.CopySelectionToClipboard();
	}
}
