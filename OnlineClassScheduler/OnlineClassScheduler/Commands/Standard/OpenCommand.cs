﻿using System;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Input;
using OnlineClassScheduler.Models;
using Windows.System;

namespace OnlineClassScheduler.Commands.Standard;

public sealed class OpenCommand : CustomStandardUICommand
{
	public override ICommand Get()
	{
		var command = new StandardUICommand(StandardUICommandKind.Open);
		command.CanExecuteRequested += this.OnCanExecuteRequested;
		command.ExecuteRequested += this.OnExecuteRequested;
		return command;
	}

	private void OnCanExecuteRequested(XamlUICommand sender, CanExecuteRequestedEventArgs args)
	{
		if (args.Parameter == null) return;

		var dataGrid = (DataGrid)args.Parameter;
		args.CanExecute = dataGrid.SelectedItem != null;
	}

	protected override async void OnExecuteRequested(XamlUICommand sender, ExecuteRequestedEventArgs args)
	{
		var dataGrid = (DataGrid)args.Parameter;
		var onlineClass = (OnlineClass)dataGrid.SelectedItem;
		var uri = new Uri(onlineClass.Uri);
		_ = await Launcher.LaunchUriAsync(uri);
	}
}
