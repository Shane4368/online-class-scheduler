﻿using System.Linq;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Input;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;

namespace OnlineClassScheduler.Commands.Standard;

public sealed class DeleteCommand : CustomStandardUICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public DeleteCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public override ICommand Get()
	{
		var command = new StandardUICommand(StandardUICommandKind.Delete);
		command.ExecuteRequested += this.OnExecuteRequested;
		return command;
	}

	protected override void OnExecuteRequested(XamlUICommand sender, ExecuteRequestedEventArgs args)
	{
		var dataGrid = (DataGrid)args.Parameter;
		var count = this._viewModel.RemoveOnlineClasses(dataGrid.SelectedItems.Cast<OnlineClass>());
		((MainWindow)App.MainWindow).ShowUnsavedIndicator(count > 0);
	}
}
