﻿using System.Windows.Input;
using Microsoft.UI.Xaml.Input;

namespace OnlineClassScheduler.Commands.Standard;

public abstract class CustomStandardUICommand
{
	public abstract ICommand Get();
	protected abstract void OnExecuteRequested(XamlUICommand sender, ExecuteRequestedEventArgs args);
}
