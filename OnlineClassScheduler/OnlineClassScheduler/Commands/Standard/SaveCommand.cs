﻿using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Input;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;

namespace OnlineClassScheduler.Commands.Standard;

public sealed class SaveCommand : CustomStandardUICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public SaveCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public override ICommand Get()
	{
		var command = new StandardUICommand(StandardUICommandKind.Save);
		command.ExecuteRequested += this.OnExecuteRequested;
		return command;
	}

	protected override void OnExecuteRequested(XamlUICommand sender, ExecuteRequestedEventArgs args)
	{
		var dataGrid = (DataGrid)args.Parameter;
		_ = dataGrid.CommitEdit();

		this._viewModel.SaveOnlineClasses();
		((MainWindow)App.MainWindow).ShowUnsavedIndicator(false);
	}
}
