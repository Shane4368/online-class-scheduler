﻿using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Input;

namespace OnlineClassScheduler.Commands.Standard;

public sealed class CopyCommand : CustomStandardUICommand
{
	public override ICommand Get()
	{
		var command = new StandardUICommand(StandardUICommandKind.Copy);
		command.CanExecuteRequested += this.OnCanExecuteRequested;
		command.ExecuteRequested += this.OnExecuteRequested;
		return command;
	}

	private void OnCanExecuteRequested(XamlUICommand sender, CanExecuteRequestedEventArgs args)
	{
		if (args.Parameter == null) return;

		var dataGrid = (DataGrid)args.Parameter;
		args.CanExecute = dataGrid.SelectedItem != null;
	}

	protected override void OnExecuteRequested(XamlUICommand sender, ExecuteRequestedEventArgs args)
	{
		var dataGrid = (DataGrid)args.Parameter;

		var textBlock = new TextBlock
		{
			Text = dataGrid.SelectedItem.ToString(),
			IsTextSelectionEnabled = true
		};

		textBlock.SelectAll();
		textBlock.CopySelectionToClipboard();
	}
}
