﻿using System;
using System.Windows.Input;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using OnlineClassScheduler.Configuration;
using OnlineClassScheduler.ViewModels;
using Windows.Storage;
using Windows.Storage.Pickers;
using WinRT.Interop;

namespace OnlineClassScheduler.Commands;

public sealed class ExportIcsCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public ExportIcsCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		return this._viewModel.OnlineClasses != null;
	}

	public async void Execute(object? parameter)
	{
		var windowHandle = WindowNative.GetWindowHandle(App.MainWindow);
		var filePicker = new FileSavePicker();

		InitializeWithWindow.Initialize(filePicker, windowHandle);

		filePicker.FileTypeChoices.Add("iCalendar", new[] { ".ics" });
		filePicker.SuggestedFileName = "timetable.ics";

		var file = await filePicker.PickSaveFileAsync();

		if (file == null) return;

		var calendar = new Calendar();
		var recurrence = new RecurrencePattern(FrequencyType.Weekly);

		foreach (var onlineClass in this._viewModel.OnlineClasses)
		{
			var startDate = onlineClass.GetTriggerStartBoundary();
			var summary = $"{onlineClass.ModuleCode} {onlineClass.ModuleName} {onlineClass.Type}";

			var minutesAhead = onlineClass.MinutesAhead == 0
				? this._viewModel.AppSettings.MinutesAhead
				: onlineClass.MinutesAhead;

			var description = $"Exported from {AppConfiguration.PackageName}\n" +
				$"<a href=\"{onlineClass.Uri}\">{onlineClass.Uri}</a>\n" +
				$"{onlineClass.Occurrence} {onlineClass.LecturerName}";

			var alarm = new Alarm
			{
				Action = AlarmAction.Display,
				Description = "Go to class",
				Trigger = new Trigger(TimeSpan.FromMinutes(-minutesAhead))
			};

			var calendarEvent = new CalendarEvent
			{
				Uid = onlineClass.Id,
				Start = new CalDateTime(startDate),
				Summary = summary.Trim(),
				Description = description.Trim(),
				RecurrenceRules = { recurrence },
				Alarms = { alarm }
			};

			calendar.Events.Add(calendarEvent);
		}

		CachedFileManager.DeferUpdates(file);

		var serializer = new CalendarSerializer();

		await FileIO.WriteTextAsync(file, serializer.SerializeToString(calendar));
		_ = await CachedFileManager.CompleteUpdatesAsync(file);
	}
}
