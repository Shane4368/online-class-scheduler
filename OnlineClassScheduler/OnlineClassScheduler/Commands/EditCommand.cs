﻿using System;
using System.Windows.Input;
using CommunityToolkit.WinUI.UI.Controls;
using Microsoft.UI.Xaml.Controls;
using OnlineClassScheduler.Models;
using OnlineClassScheduler.ViewModels;
using OnlineClassScheduler.Views;
using OnlineClassScheduler.Views.Dialogs;

namespace OnlineClassScheduler.Commands;

public sealed class EditCommand : ICommand
{
	private readonly OnlineClassesViewModel _viewModel;

	public EditCommand(OnlineClassesViewModel viewModel) => this._viewModel = viewModel;

	public event EventHandler? CanExecuteChanged;

	public bool CanExecute(object? parameter)
	{
		if (parameter == null) return true;

		var dataGrid = (DataGrid)parameter;
		return dataGrid.SelectedItem != null;
	}

	public async void Execute(object? parameter)
	{
		var dataGrid = (DataGrid)parameter!;
		var onlineClass = (OnlineClass)dataGrid.SelectedItem;
		var onlineClassCopy = new OnlineClass(onlineClass);

		// temporarily remove from collection to prevent program from crashing
		var index = this._viewModel.OnlineClasses.IndexOf(onlineClass);
		this._viewModel.OnlineClasses.RemoveAt(index);

		var dialog = new OnlineClassModalDialog(onlineClassCopy)
		{
			Title = "Edit Online Class",
			PrimaryButtonText = "Save edit",
			UseEditMode = true
		};

		var result = await dialog.ShowAsync();

		if (result != ContentDialogResult.Primary)
		{
			this._viewModel.OnlineClasses.Insert(index, onlineClass);
			return;
		}

		dataGrid.IsReadOnly = true;

		onlineClass.Occurrence = onlineClassCopy.Occurrence;
		onlineClass.ModuleCode = onlineClassCopy.ModuleCode;
		onlineClass.ModuleName = onlineClassCopy.ModuleName;
		onlineClass.LecturerName = onlineClassCopy.LecturerName;
		onlineClass.Type = onlineClassCopy.Type;
		onlineClass.Day = onlineClassCopy.Day;
		onlineClass.Time = onlineClassCopy.Time;
		onlineClass.Uri = onlineClassCopy.Uri;
		onlineClass.MinutesAhead = onlineClassCopy.MinutesAhead;

		this._viewModel.OnlineClasses.Insert(index, onlineClass);

		dataGrid.IsReadOnly = false;

		((MainWindow)App.MainWindow).ShowUnsavedIndicator();
	}
}
