﻿using System.Configuration;
using Windows.ApplicationModel;
using Windows.Storage;

namespace OnlineClassScheduler.Configuration;

public static class AppConfiguration
{
	static AppConfiguration()
	{
		var package = Package.Current;
		var packageIdentity = package.Id;
		var packageVersion = packageIdentity.Version;

		Author = package.PublisherDisplayName;
		PackageName = packageIdentity.Name;
		PackageFamilyName = packageIdentity.FamilyName;
		PackageInstalledLocation = package.InstalledLocation;
		PackageVersion = $"{packageVersion.Major}.{packageVersion.Minor}.{packageVersion.Build}.{packageVersion.Revision}";

		AuthorUrl = ConfigurationManager.AppSettings.Get("author-url")!;
		DefaultOnlineClassUrl = ConfigurationManager.AppSettings.Get("default-onlineclass-url")!;
	}

	public static readonly string Author;
	public static readonly string AuthorUrl;
	public static readonly string PackageName;
	public static readonly string PackageFamilyName;
	public static readonly StorageFolder PackageInstalledLocation;
	public static readonly string PackageVersion;
	public static readonly string DefaultOnlineClassUrl;
}
