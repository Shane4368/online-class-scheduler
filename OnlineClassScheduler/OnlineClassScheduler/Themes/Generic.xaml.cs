﻿using Microsoft.UI.Xaml;

namespace OnlineClassScheduler.Themes;

public sealed partial class Generic : ResourceDictionary
{
	// Ignore this error; still compiles
	public Generic() => this.InitializeComponent(); // CS1061
}
