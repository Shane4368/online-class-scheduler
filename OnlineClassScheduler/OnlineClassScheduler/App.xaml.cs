﻿using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.UI.Xaml;
using OnlineClassScheduler.Configuration;
using OnlineClassScheduler.Services;
using OnlineClassScheduler.Views;

// To learn more about WinUI, the WinUI project structure,
// and more about our project templates, see: http://aka.ms/winui-project-info.

namespace OnlineClassScheduler;

/// <summary>
/// Provides application-specific behavior to supplement the default Application class.
/// </summary>
public sealed partial class App : Application
{
	public static Window MainWindow { get; private set; } = null!;

	public static QueryCollection QueryCollection { get; private set; } = null!;
	/// <summary>
	/// Initializes the singleton application object. This is the first line of authored code
	/// executed, and as such is the logical equivalent of main() or WinMain().
	/// </summary>
	public App()
	{
		this.InitializeComponent();
	}

	/// <summary>
	/// Invoked when the application is launched normally by the end user. Other entry points
	/// will be used such as when the application is launched to open a specific file.
	/// </summary>
	/// <param name="args">Details about the launch request and process.</param>
	protected override void OnLaunched(LaunchActivatedEventArgs args)
	{
		var arguments = Environment.GetCommandLineArgs();

		if (arguments.Length == 2)
			this.HandleCommandLineArguments(arguments);

		QueryCollection = new QueryCollection();

		MainWindow = new MainWindow();
		MainWindow.Activate();
	}

	private void HandleCommandLineArguments(string[] args)
	{
		if (args[1] == "--start-background-service")
		{
			var connection = DatabaseFactory.getConnection();
			var dataSource = connection.DataSource;
			var packageFamilyName = AppConfiguration.PackageFamilyName;

			connection.Dispose();

			Process.Start(new ProcessStartInfo
			{
				FileName = BackgroundService.GetExecutablePath(),
				Arguments = $"{packageFamilyName} {dataSource}",
				CreateNoWindow = true
			})!.Dispose();

			Environment.Exit(0);
		}

		OnlineClassQuery.SelectFromDatabase(args[1]).ShowToastNotification();
		Thread.Sleep(500); // apparently, sending toasts take a little time
		Environment.Exit(0);
	}
}
